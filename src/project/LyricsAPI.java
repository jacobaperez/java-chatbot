package project;

import com.google.gson.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Jake
 *
 */
public class LyricsAPI {
	private static String lyricURL = "http://lyric-api.herokuapp.com/api/find/";

	public static String[] getSongLyrics(String[] songName) throws IOException {
//		System.out.println("APIAPI>>>" + songName);
		// TODO: if artist is multiple names add, loop
		// to catch entire name (see below for song)

		String artist = songName[0] + "/";
		String song = "";
		for (int i = 1; i < songName.length; i++) {
			song += songName[i] + "%20";
		}
		String search = lyricURL + artist + song;
		StringBuilder response = new StringBuilder();

		try {
			URL url = new URL(search);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			// Keep track of current line
			String line;

			// get everything from stream.
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
			reader.close();

			JsonObject respObj = new JsonParser().parse(response.toString()).getAsJsonObject();
			String words = respObj.get("lyric").getAsString();
			String[] wordz = words.split("\\r?\\n");

			return wordz;

		} catch (MalformedURLException e) {
			e.printStackTrace();
			String[] nada = {};
			return nada;
		}
	}
}
