package project;

import com.google.gson.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Jake
 *
 */
public class WeatherAPI {
	private static String apiKey = "&APPID=a92d8bd431db479841836c71024b1848";
	private static String units = "&units=imperial";
	private static String weatherURL = "http://api.openweathermap.org/data/2.5/weather?q=";

	public static String getWeather(String zipcode) throws IOException {
		String search = weatherURL + zipcode + ",us" + units + apiKey;
		StringBuilder response = new StringBuilder();

		try {
			URL url = new URL(search);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			// Keep track of current line
			String line;

			// get everything from stream.
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
			reader.close();

			JsonObject respObj = new JsonParser().parse(response.toString()).getAsJsonObject();
			JsonElement city = respObj.get("name");
			JsonObject temps = respObj.getAsJsonObject("main");
		
			String weatherReport =  city + " Temperatures - " +
									"Current: " + temps.get("temp").getAsInt() + "\u00B0F, " +
									"Low: " + temps.get("temp_min").getAsInt() +  "\u00B0F, " +
									"High: " + temps.get("temp_max").getAsInt() + "\u00B0F";
			return weatherReport;

		} catch (MalformedURLException e) {
			e.printStackTrace();
			return "Couldn't get temperature, please try again.";
		}

	}

}
