package project;

import java.io.IOException;
import java.util.Arrays;

import org.jibble.pircbot.*;

/**
 * @author Jake
 *
 */
public class Bot extends PircBot {

	public Bot() {
		this.setName("TotallyNotABot");
	}

	public void onMessage(String channel, String sender, String login, String hostname, String message) {
		// Make message easier to work with.
		message = message.toLowerCase();

		if (message.contains("weather")) {
			String[] words = message.split(" ");
			String res;
			if (words.length == 2) {
				try {
					res = WeatherAPI.getWeather(words[1]);
					sendMessage(channel, res);
				} catch (IOException e) {
					e.printStackTrace();
					sendMessage(channel, "Sorry I didn't get that, try asking like (weather ZIPCODE)");
				}
			}
		}

		// Maybe change to else to prevent sending two messages
		if (message.contains("what") && message.contains("time")) {
			String time = new java.util.Date().toString();
			sendMessage(channel, sender + ": I kick your butt on " + time);
		}

		if (message.contains("lyrics")) {
			String[] words = message.split(" ");
			String[] songName = Arrays.copyOfRange(words, 1, words.length);
			String[] res;
			try {
				res = LyricsAPI.getSongLyrics(songName);
				sendMessage(channel, "LYRICS: \n");
				for (int i = 0; i < res.length; i++) {
					sendMessage(channel, res[i]);
				}
				sendMessage(channel, "End :)");
			} catch (IOException e) {
				e.printStackTrace();
				sendMessage(channel, "Sorry I didn't get that, try asking like (lyrics SONG_NAME)");
			}
		}
	}

}
